# my-first-terragrunt

- This is my first project using Terragrunt and Terraform, happy to be doing this!

You need to customize:

- .gitlab-ci.yml
```
AWS_ACCESS_KEY_ID: $TF_VAR_access_key
AWS_SECRET_ACCESS_KEY: $TF_VAR_secret_key
AWS_DEFAULT_REGION: us-east-1
```
- terragrunt.hcl
```
bucket = "terraform-state-gortega"
```