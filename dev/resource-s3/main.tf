resource "aws_s3_bucket" "terraform-myfirstapp-database" {
    bucket = "terraform-myfirstapp-database"
    acl = "private"

    tags = {
      "Name" = "terraform-myFirstApp"
      "Environment" = "Dev"
    }
  
}