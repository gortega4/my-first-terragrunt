remote_state {
    backend = "s3"
    generate = {
        path =          "backend.tf"
        if_exists =     "overwrite_terragrunt"
    }
    config = {
        bucket = "terraform-state-gortega"

        key = "${path_relative_to_include()}/terraform.tfstate"
        region              = "sa-east-1"
        encrypt             = true
        dynamodb_table      = "my-lock-table"
    }
}

generate "provider" {
    path = "provider.tf"
    if_exists = "overwrite_terragrunt"
    contents = <<EOF
provider "aws" {
  region = var.aws_region
}
EOF
}

generate "variables" {
    path = "variables.tf"
    if_exists = "overwrite_terragrunt"

    contents = <<EOF
variable "aws_region" {
  description = "The AWS region to create things in."
  default     = "sa-east-1"
}
EOF
}